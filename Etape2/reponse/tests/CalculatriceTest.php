<?php // CalculatriceTest.php

class CalculatriceTest extends PHPUnit_Framework_TestCase {
    public function testInstances()
    {
        new Calculatrice;
    }

    public function testResultatDefaultsZero()
    {
        $calc = new Calculatrice;
        $this->assertSame(0,$calc->getResultat());
    }

    public function testAjouterNombre()
    {
        $calc = new Calculatrice;
        $calc->ajouter(2);
        $this->assertEquals(2,$calc->getResultat());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testValeurNumerique()
    {
        $calc = new Calculatrice;
        $calc->ajouter('un chiffre');
    }
}
?>
