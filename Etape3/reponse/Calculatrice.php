<?php
// TODO : A vous de jouer
//
class Calculatrice 
{
    protected $resultat=0; 

    public function getResultat()
    {
        return $this->resultat;
    }

    public function ajouter() 
    {
        foreach (func_get_args() as $nombre)
        {
            if ( ! is_numeric($nombre))
                throw new InvalidArgumentException;
            $this->resultat += $nombre;
        }
    }
}
?>
