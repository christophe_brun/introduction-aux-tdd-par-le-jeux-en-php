<?php
    class Calculatrice {

        public function __construct() {
            $this->Result = 0;
        }

        public function getResultat() {
            return $this->Result;
        }


        /*
         * PRIVATE
         */

        private function calculer( $signe, array $nbs ) {
            foreach( $nbs as $nb ) {
                if ( ! is_int( $nb ) ) {
                    throw new InvalidArgumentException( $nb . ' is not an int! :)' );
                }
                
                switch( $signe ) {
                
                    case 'ajouter':
                        $this->Result += $nb;
                        break;
                    
                    case 'soustraire':
                        $this->Result -= $nb;
                        break;
                    
                }
                    
            }
        }


        /*
         * OPERATION
         */

        public function ajouter( ) {
            self::calculer( __FUNCTION__, func_get_args() );
        }

        public function soustraire( ) {
            self::calculer( __FUNCTION__, func_get_args() );
        }

    }
?>
