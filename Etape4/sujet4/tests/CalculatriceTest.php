<?php // CalculatriceTest.php

class CalculatriceTest extends PHPUnit_Framework_TestCase {

    public function setUp()
    {
        $this->calc = new Calculatrice;
    }

    public function testInstances()
    {
        new Calculatrice;
    }

    public function testResultatDefaultsZero()
    {
        $this->assertSame(0,$this->calc->getResultat());
    }

    public function testAjouterNombre()
    {
        $this->calc->ajouter(2);
        $this->assertEquals(2,$this->calc->getResultat());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testValeurNumerique()
    {
        $this->calc->ajouter('un chiffre');
    }

    public function testAjouterPlusieursValeurs()
    {
        $this->calc->ajouter(1,2,3,4,5);
        $this->assertEquals(15,$this->calc->getResultat());
    }

    public function testSoustraire()
    {
        $this->calc->soustraire(4);
        $this->assertEquals(-4,$this->calc->getResultat());
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testValeurNumeriqueSoustraction()
    {
        $this->calc->soustraire('bonjour');
    }

    public function testSoustrairePlusieursValeurs()
    {
        $this->calc->soustraire(4,2);
        $this->assertEquals(-6,$this->calc->getResultat());
    }
}

?>
