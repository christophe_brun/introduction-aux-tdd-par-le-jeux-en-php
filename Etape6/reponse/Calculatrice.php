<?php
// TODO : A vous de jouer
//
class Calculatrice 
{
    protected $resultat=0; 

    protected function operation(array $nombres,$operateur)
    {
        foreach ($nombres as $nombre)
        {
            if ( ! is_numeric($nombre))
                throw new InvalidArgumentException;
            switch ($operateur)
            {
            case '+':
                $this->resultat+=$nombre;
                break;
            case '-':
                $this->resultat-=$nombre;
                break;
            }
        }
    }

    public function getResultat()
    {
        return $this->resultat;
    }

    public function ajouter() 
    {
        $this->operation(func_get_args(),'+');
    }

    public function soustraire()
    {
        $this->operation(func_get_args(),'-');
    }

    public function initResultat($nombre)
    {
        $this->resultat=$nombre;
        return $this->resultat;
    }
}
?>
