# README

Un exemple de TDD. 
L'idée étant de donner à une équipe semaine par semaine une étape d'un classe Calculatrice. 

Pas de spécifications, juste des tests.

Les règles : 

* pas de contraintes
* pas de spécifications
* retour du code si passage des tests vert.
 

A l'issue, un bilan reprenant le code de chacun (de façon anonyme) et retro avec toute l'équipe