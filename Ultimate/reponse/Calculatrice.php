<?php

interface Operation
{
    public function go($nombre,$courrant);
}


class Addition implements Operation {
    public function go($nombre, $courrant)
    {
        return $courrant + $nombre;
    }
}

class Multiplication implements Operation {
    public function go($nombre,$courrant)
    {
        if (is_null($courrant))
            return $nombre;
        return $courrant * $nombre;
    }
}

class Soustraction implements Operation {
    public function go($nombre, $courrant)
    {
        return $courrant - $nombre;
    }
}


class Calculatrice 
{
    protected $resultat;
    protected $valeurs = [];
    protected $operation; 

    public function getResultat()
    {
        return $this->resultat;
    }

    public function setValeurs()
    {
        $this->valeurs = func_get_args();
    }

    public function setOperation(Operation $operation)
    {
        $this->operation = $operation;
    }

    public function calcul() 
    {
        foreach ($this->valeurs as $nombre)
        {
            if ( ! is_numeric($nombre))
                throw new InvalidArgumentException;
            $this->resultat = $this->operation->go($nombre,$this->resultat);
        }
        return $this->resultat;
    }
}
?>
