<?php // CalculatriceTest.php


class CalculatriceTest extends PHPUnit_Framework_TestCase {

    public function setUp()
    {
        $this->calc = new Calculatrice;
    }

    public function testResultatNullParDefaut()
    {
        $this->assertNull($this->calc->getResultat());
    }

    public function testAjoutNombreViaMock()
    {
        $mock = Mockery::mock('Operation');
        $mock->shouldReceive('go')
            ->once()
            ->with(5,0)
            ->andReturn(5);
        $this->calc->setValeurs(5);
        $this->calc->setOperation($mock);
        $resultat = $this->calc->calcul();
        $this->assertEquals(5,$resultat);
    }

    public function testAjouterNombre()
    {
        $this->calc->setValeurs(5);
        $this->calc->setOperation(new Addition);
        $resultat=$this->calc->calcul();
        $this->assertEquals(5,$resultat);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testTypeNombre()
    {
        $this->calc->setValeurs('cinq');
        $this->calc->setOperation(new Addition);
        $resultat=$this->calc->calcul();
    }

    public function testMultipleNombre()
    {
        $this->calc->setValeurs(1,2,3,4,5);
        $this->calc->setOperation(new Addition);
        $resultat=$this->calc->calcul();
        $this->assertEquals(15,$resultat);
        $this->assertNotEquals('Juste pour le fun',$resultat);
    }

    public function testSoustraction()
    {
        $this->calc->setValeurs(4);
        $this->calc->setOperation(new Soustraction);
        $resultat = $this->calc->calcul();
        $this->assertEquals(-4,$resultat);
    }

    public function testMultiplication()
    {
        $this->calc->setValeurs(2,3,5);
        $this->calc->setOperation(new Multiplication);
        $resultat = $this->calc->calcul();
        $this->assertEquals(30,$resultat);
    }

}



?>
